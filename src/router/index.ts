import { authRouter } from '@/modules/auth/router';
import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      ...authRouter
    }
  ],
});

export default router
